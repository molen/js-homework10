let pswrdOne = document.getElementById('password1');
let pswrdTwo = document.getElementById('password2');
let confirmBtn = document.getElementById('button');
let errorText = document.createElement('p');
let eyeOpenButton = document.getElementById('eyeOpen');
let eyeClosedButton = document.getElementById('eyeClosed');

function passwordVisibility(eyeButton) {
    eyeButton.addEventListener('click', () => {
        eyeButton.classList.toggle("fa-eye-slash");
        eyeButton.classList.toggle("fa-eye");
        if (eyeButton.classList.contains('fa-eye')) {
            eyeButton.previousElementSibling.setAttribute('type', 'text')
        } else {
            eyeButton.previousElementSibling.setAttribute('type', 'password')
        }
    });
}

confirmBtn.addEventListener('click', () => {
    if (pswrdOne.value === pswrdTwo.value) {
        alert('You are welcome');
    } else {
        pswrdTwo.after(errorText);
        errorText.style = 'color: red;';
        errorText.innerText = 'Нужно ввести одинаковые значения';
    }
});

passwordVisibility(eyeOpenButton);
passwordVisibility(eyeClosedButton);